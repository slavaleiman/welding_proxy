#include "unity.h"
#include <stdint.h>
#include "welding_head.h"
#include "utils.h"

void setUp(void)
{

}

void tearDown(void)
{
}

uint32_t operation_time = 1;

uint32_t uptime(void)
{
    return operation_time++;
}

uint32_t log_files_count(void)
{
    return 234;
}

void test_whead_push_pull(void) // deprecated
{
    #define DATALEN 16
    uint8_t data[DATALEN];
	for(uint16_t i = 0; i < 43; ++i)
    {
        uint32_t value = i % DATALEN;
        data[i % DATALEN] = value; 
        memdump(data, DATALEN, 16);
        whead_on_message(data, DATALEN);
    }
    // uint32_t read_index = 0;
    // uint8_t *msg;
    // uint8_t len;

    // while(whead_buffer_get_item(read_index, &msg, &len) > 0)
    // {
    //     printf("msg: %x %x %x %x %x %x %x %x\t", msg[0], msg[1], msg[2], msg[3], msg[4], msg[5], msg[6], msg[7]);
    //     printf("read index: %d\n", read_index);

    //     ++read_index;
    //     if(read_index >= WHEAD_BUFFER_SIZE)
    //     {
    //         read_index = 0;
    //     }
    // }
}

void test_whead_overload(void)
{
    // при записи в буфер не должно проверяться успел ли читатель
    // и здесь не будет проблем

    #define DATALEN 16
    uint8_t data[DATALEN];

    for(uint32_t i = 0; i < 200; ++i)
    {
        uint32_t i_ = i % DATALEN;
        data[i % DATALEN] = i_;
        whead_on_message(data, DATALEN);
        // printf("push data: %02x %02x %02x %02x %02x %02x %02x %02x\t\n", 
                            // data[0],data[1],data[2],data[3],data[4],data[5],data[6],data[7]);
    }
    // uint32_t read_index = 0;
    // uint8_t *msg;
    // uint8_t len;
    // while(whead_buffer_get_item(read_index, &msg, &len) > 0)
    // {
    //     printf("msg: %02x %02x %02x %02x %02x %02x %02x %02x\t", msg[0], msg[1], msg[2], msg[3], msg[4], msg[5], msg[6], msg[7]);
    //     printf("read index: %d\n", read_index);
    //     ++read_index;
    //     if(read_index >= WHEAD_BUFFER_SIZE)
    //     {
    //         read_index = 0;
    //         break;
    //     }
    // }
    // TEST_ASSERT_EQUAL();
}

int main(void)
{
    UNITY_BEGIN();
    RUN_TEST(test_whead_push_pull);
    // RUN_TEST(test_whead_overload);
    
    return UNITY_END();
}
