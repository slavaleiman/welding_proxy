CLEANUP = rm -f
MKDIR = mkdir -p
TARGET_EXTENSION=

C_COMPILER=gcc

UNITY_ROOT=../../Unity
SRC_DIR=..

CFLAGS=-std=c99
# CFLAGS += -Wall
CFLAGS += -Wextra
# CFLAGS += -Wpointer-arith
CFLAGS += -Wcast-align
CFLAGS += -Wwrite-strings
CFLAGS += -Wswitch-default
CFLAGS += -Wunreachable-code
CFLAGS += -Winit-self
CFLAGS += -Wmissing-field-initializers
CFLAGS += -Wno-unknown-pragmas
CFLAGS += -Wstrict-prototypes
CFLAGS += -Wundef
CFLAGS += -Wold-style-definition -g3
CFLAGS += -m32
# CFLAGS += -Werror

TARGET=test_whead

SRC_FILES=$(UNITY_ROOT)/src/unity.c \
	$(SRC_DIR)/Core/Src/welding_head.c \
	$(SRC_DIR)/Core/Src/utils.c \
	test_whead.c

INC_DIRS=-I../src -I$(UNITY_ROOT)/src \
	-I$(SRC_DIR)/ \
	-I$(SRC_DIR)/Core/Src

SYMBOLS=-DSTM32F405 -DUNITTEST=1

LIBS = -lm

all: clean test_whead

test_whead: $(SRC_FILES)
	$(C_COMPILER) $(CFLAGS) $(INC_DIRS) $(SYMBOLS) $(SRC_FILES) -o $(TARGET) $(LIBS)
	- ./$(TARGET)
clean:
	$(CLEANUP) $(TARGET) 
