
UNITY_ROOT=../../Unity
SRC_DIR=../Core/Src

TARGET=test_fbuff

CLEANUP = rm -f
MKDIR = mkdir -p
TARGET_EXTENSION=

C_COMPILER=gcc


CFLAGS=-std=c99
# CFLAGS += -Wall
CFLAGS += -Wextra
# CFLAGS += -Wpointer-arith
CFLAGS += -Wcast-align
CFLAGS += -Wwrite-strings
CFLAGS += -Wswitch-default
CFLAGS += -Wunreachable-code
CFLAGS += -Winit-self
CFLAGS += -Wmissing-field-initializers
CFLAGS += -Wno-unknown-pragmas
CFLAGS += -Wstrict-prototypes
CFLAGS += -Wundef
CFLAGS += -Wold-style-definition -g3
CFLAGS += -m32

SRC_FILES=$(UNITY_ROOT)/src/unity.c \
	$(SRC_DIR)/flash_buffer.c \
	test_flash_buffer.c

INC_DIRS= -I$(UNITY_ROOT)/src \
	-I$(SRC_DIR)/

SYMBOLS=-DSTM32F429 -DUNITTEST

LIBS = -lm

all: test_fbuff

test_fbuff: $(SRC_FILES)
	$(C_COMPILER) $(CFLAGS) $(INC_DIRS) $(SYMBOLS) $(SRC_FILES) -o $(TARGET) $(LIBS)
	- ./$(TARGET)
