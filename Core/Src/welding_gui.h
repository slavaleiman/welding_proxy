#ifndef _WELDING_GUI_H_
#define _WELDING_GUI_H_

void welding_gui_init(void);

void welding_gui_update_temp(void);
void welding_gui_update_voltage(void);
void welding_gui_update(void);

#endif
