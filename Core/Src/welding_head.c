#include <stdint.h>
#include <string.h>
#include "welding_head.h"
#include "errors.h"
#include "log_sd.h"
#include <stdio.h>
#include "circular_buffer.h"
#include "uptime.h"
#include "utils.h"

#ifndef UNITTEST
	#include "sadko.h"
	#include "stm32f4xx_hal.h"
#endif

// протоколы 3, 4 отличаются проверкой чётности
// в 4м - even
// extern usart_port_t uport7;

// отличия режимов 
#define INPUT_CBUFFER_LEN 512
struct
{
	uint8_t  message_buffer[255]; // output message
	
	// первый байт сообщения - это его длина
	 // 1byte size + 8 or 11 bytes

	uint32_t message_count;
	uint32_t saved;

}whead; // welding head data

CIRCULAR_BUFFER_DEF(input_cbuffer, INPUT_CBUFFER_LEN)

void whead_on_message(uint8_t* data, uint16_t len)
{
	++whead.message_count;
	// push to input_cbuffer 
	uint8_t id_data[256 + 16];
	uint32_t up_time = uptime();

	memcpy(&id_data[0], &up_time, 4);
	memcpy(&id_data[4], &len, 2);
	memcpy(&id_data[MSG_HEADER_LEN], data, len);
#ifdef UNITTEST
	printf("l = 0x%x len = 0x%x\n", l, len);
	memdump(id_data, len + l, 16);
#endif
#ifndef UNITTEST
	int ret = cbuffer_push_message(&input_cbuffer, id_data, len + MSG_HEADER_LEN);
	if(ret < 0)
		errors_on_error(CBUFFER_PUSH_FAIL);
	log_give_semaphore();
#endif
}

#ifndef UNITTEST
void welding_data(char* string)
{
	char* filename;
    if(!log_filename(&filename))
    {
    	sprintf(string, "сообщений принято :%ld\n"
    					"количество записей:%ld\n"
    					"лог файл: %s\n",
		    				 whead.message_count, 
		    				 log_read_index(),
		    				 filename
    				 );
    }else{
    	sprintf(string, "сообщений принято   :%ld\n"
    					"лог файл: ОШИБКА", whead.message_count);
    }
}
#endif
