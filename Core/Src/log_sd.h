#ifndef __LOG_SD__
#define __LOG_SD__ 1

#include <stdbool.h>

#define LOG_DIR "/log"
#define LOG_MAX_FILE_NUM 10000
#define LOG_FILE_MAX_LINES 10000

#define LOG_STACK_SIZE 512
#define LOG_PRIORITY (configMAX_PRIORITIES-3)

void start_logger_task(void const *parameter);
bool logger_is_init(void);
int  log_filename(char** name);

uint32_t log_read_index(void);
void     log_give_semaphore(void);
uint32_t log_current_file_num(void);
bool 	 log_is_init(void);

// int sd_read_string(char* filename, uint8_t* data, uint16_t len);
// int sd_write_string(char* filename, uint8_t* data, uint16_t len);

#endif
