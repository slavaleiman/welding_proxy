#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <stdlib.h>
#include "ff.h"
#include "log_sd.h"
#include "uptime.h"
#include "welding_head.h"
#include "errors.h"
#include "circular_buffer.h"
#include "sadko.h"
#include "sdcard.h"

DIR dir;
FILINFO fno;

SemaphoreHandle_t logger_semaphore_handle;
StaticSemaphore_t logger_semaphore_buf;

struct 
{
    FIL file;
    uint32_t read_index;
    char     filename[64];
    uint32_t lines_count;
    bool     is_opened;
    uint32_t file_num;
    bool     is_init;
    bool     sync_flag;
}logger;

bool log_is_init(void)
{
    return logger.is_init;
}

uint32_t log_read_index(void)
{
    return logger.read_index;
}

void load_settings(void)
{
    // res = f_open(&logger.file, ".settings", FA_READ | FA_WRITE | FA_OPEN_ALWAYS);
    // if(res)
    // {
    //     return -1;
    // }
    // get last uptime
}

static int32_t get_free_file_number(void)
{
    FRESULT res;

    uint32_t max_file_num = 0;
    uint32_t min_file_num = LOG_MAX_FILE_NUM;

    TCHAR* path = LOG_DIR;

    res = f_opendir(&dir, path);
    if(res != FR_OK)
    {
        return -16;
    }
    for(;;)
    {
        res = f_readdir(&dir, &fno);
        if (res != FR_OK || fno.fname[0] == 0)
            break;

        if(!(fno.fattrib & AM_DIR))
        {
            if(!strncmp("welding_", fno.fname, 7))
            {
                char* endptr;
                uint32_t curr_proj_num = strtol(&fno.fname[8], &endptr, 10);

                if(curr_proj_num > max_file_num) // projects sorted by name
                    max_file_num = curr_proj_num;
                if(curr_proj_num < min_file_num) // projects sorted by name
                    min_file_num = curr_proj_num;
            }
        }
    }
    uint8_t project_num = 1;
    if(min_file_num < LOG_MAX_FILE_NUM && min_file_num > 1)
        project_num = 1;
    else if(max_file_num > 0)
        project_num = max_file_num + 1;

    f_closedir(&dir);

    return project_num;
}

int log_filename(char** name)
{
    if(!logger.filename[0])
        return -1;
    *name = logger.filename;
    return 0;
}

int gen_filename(char** name)
{
    if(!strlen(logger.filename))
    {
        // int count = 0;
        int32_t ret = get_free_file_number();

        if (ret < 0)
            return ret;
        else{
            logger.file_num = ret;
        }
        sprintf(logger.filename, "%s/welding_%04d.log", LOG_DIR, (int)logger.file_num);
    }
    *name = logger.filename;
    return 0;
}

int log_dir(void)
{
    FRESULT res = f_stat(LOG_DIR, 0);
    if(res == FR_NO_FILE) {
        res = f_mkdir(LOG_DIR);
        if(res != FR_OK) return res;
        res = f_stat(LOG_DIR, 0);
        if(res != FR_OK) return res;
    }
    return res;
}

int write_message(uint8_t* data, uint16_t len)
{
    FRESULT res;
    UINT bw;

    if(logger.lines_count >= LOG_FILE_MAX_LINES)
    {
        logger.is_opened = false;
        f_close(&logger.file);
        logger.lines_count = 0;
        memset(logger.filename, 0, sizeof(logger.filename)); // this wil help to open new file
    }

    char* filename;
    if(gen_filename(&filename) < 0)
        return -1;

    if(!logger.is_opened)
    {
        res = f_open(&logger.file, filename, FA_WRITE | FA_OPEN_APPEND);
        if(res != FR_OK) return res;
        logger.is_opened = true;
    }
    // это бинарный файл
    res = f_write (&logger.file, (uint8_t*)data, len, &bw);
    if(res != FR_OK) return res;

    return res;
}

int open_log_file(void)
{
    FRESULT res;
    int ret = 0;
    char* filename;
    sd_init();
    // if(xSemaphoreTake(sdcard_mutex_handle(), portMAX_DELAY) == pdTRUE)
    if(1)
    {
        do{
            if(sd_mount())
            {
                ret = -1;
                break;
            }
            res = log_dir();
            if(res != FR_OK)
            {
                ret = -1;
                break;
            }
            if(gen_filename(&filename) < 0){
                ret = -1;
                break;
            }
        }while(0);
        // xSemaphoreGive(sdcard_mutex_handle());
    }
    return ret;
}

void log_give_semaphore(void)
{
    xSemaphoreGive(logger_semaphore_handle);
}

void file_sync(void)
{
    if(logger.is_opened)
    {
        FRESULT res = f_sync(&logger.file);
        if(res == FR_OK)
            logger.sync_flag = false;
    }
}

int log_rotate(void)
{
    // f_unlink("/log/welding_1001.log");
    // посчитать все файлы
    // если их больше 1000 
        // удалить файл с самый первый файл
    return 0;
}

extern circular_buffer_t input_cbuffer;

void start_logger_task(void const *parameter)
{
    uint8_t message[256];

    logger_semaphore_handle = xSemaphoreCreateBinaryStatic(&logger_semaphore_buf);

    open_log_file();
    logger.is_init = true;
    logger.sync_flag = false;
    for(;;)
    {
        if(xSemaphoreTake(logger_semaphore_handle, 10000) == pdTRUE)
        {
            int ret = 0;
            do
            {
                // read from input cbuffer
                int len = cbuffer_pop_message(&input_cbuffer, message);
                if(len > 0)
                {
                    if(1) //xSemaphoreTake(sdcard_mutex_handle(), portMAX_DELAY) != pdTRUE)
                    {
                        ret = write_message(message, len);
                        if(!ret)
                        {
                            ++logger.read_index;
                            ++logger.lines_count;
                            logger.sync_flag = true;
                        }
                        // xSemaphoreGive(sdcard_mutex_handle());
                    }else{
                        errors_on_error(SDCARD_WRITE_ERROR);
                    }                    
                }
                if(ret < 0)
                {
                    logger.read_index = 0; // drop on overflow write index
                }
            }while(ret > 0);
        }else{
            file_sync();
            log_rotate();
        }
        osDelay(1);
    }
}

uint32_t log_current_file_num(void)
{
    return logger.file_num;
}
