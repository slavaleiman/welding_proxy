#ifndef __SADKO__
#define __SADKO__ 1

#include "usart_port.h"
#include "modbus.h"
#include "errors.h"
#include "welding_head.h"
#include "CRC.h"

#define SADKO_MODBUS_ADDR 0x20

// from port
void sadko_on_message(uint8_t* data, uint16_t len);

void sadko_set_modbus_addr(uint8_t addr);
void start_sadko_task(void const *parameter);
int sadko_update(void);
int sadko_init(usart_port_t* port, uint8_t addr);
void sadko_set_output_index(uint32_t id);
void sadko_data(char* string);

#endif
