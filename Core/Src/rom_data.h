
#ifndef __ROM_DATA__
#define __ROM_DATA__ 1

#include "main.h"
#include <stdbool.h>

#define OPTIONS_IN_FLASH  1
#ifdef OPTIONS_IN_FLASH

// Sector 12 0x08100000 - 0x08103FFF 16 Kbytes
// Sector 13 0x08104000 - 0x08107FFF 16 Kbytes
// Sector 14 0x08108000 - 0x0810BFFF 16 Kbytes
// Sector 15 0x0810C000 - 0x0810FFFF 16 Kbytes

    #define DATA_SECTOR1       		12 // FLASH_Sector_12
    #define DATA_SECTOR2       		13
    #define DATA_SECTOR3       		14
    #define DATA_SECTOR4       		15
    #define ROM_DATA_CONFIG_ADDR 	(0x08100000)
#endif

int 	rom_write(uint8_t* data, uint32_t len);
int 	rom_read(uint8_t* data);

void 	rom_load_data(void);
void 	rom_save_data(void);

void 	rom_data_changed(void);
bool 	rom_data_is_changed(void);

void 	rom_init(void);

#endif /*__ROM_DATA__*/
