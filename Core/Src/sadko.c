#include "sadko.h"
#include "usart_port.h"
#include "modbus.h"
#include "errors.h"
#include "welding_head.h"
#include "CRC.h"
#include "circular_buffer.h"
#include "sdcard.h"
#include "ff.h"
#include "log_sd.h"
#include <stdio.h>
#include <stdlib.h>

#define SADKO_CBUFFER_SIZE 1024
#define SADKO_MAX_MSG_LEN (125 * 2) // почему 125 см. doc/mb_protocol.doc (протокол садко)
#define SADKO_MSG_HEADER_LEN 4 // 2 - len, 2 - msg_num
extern usart_port_t uport7; // output

enum{
    DEVICE_ID_REG   = 0x1000,
    FMW_VER_REG     = 0x1001,
    MSG_LEN_REG,        // длина сообщений в байтах
    MSG_REGS_NUM_REG,   // количество регистров с сообщениями или 0
    MSG_AVAILABLE_REG,  // количество сообщений доступно для чтения
    STATUS_REG1,        // статус
    STATUS_REG2,
    MESSAGE_LEN_REG     = 0x2000,
    MESSAGE_INDEX_REG   = 0x2001,
    MESSAGE_DATA_REG    = 0x2002,
}input_regs_e;

// здесь лежат сообщения на выходе 
// буфер должен обновляться после кадого подтверждения отправки

CIRCULAR_BUFFER_DEF(sadko_cbuffer, SADKO_CBUFFER_SIZE)

struct
{
	usart_port_t *uport;
    FIL      file;
    FILINFO  fno;
    char     filename[128];

    // эти параметры сохраняются в файла ".sadko" 2 + 4 + 2 = 8 байт
    uint16_t file_num;      // номер файла с которого продолжать чтение после выключения
    uint32_t file_read_ptr; // позиция с которой продолжать чтение после выключения
    uint16_t output_id;     // порядковый номер отправленного файла

    bool     is_file_opened;

    uint8_t  message[255];      // в этот буффер читаем из карты по одному сообщению
    uint16_t message_length;    // длина данных для отправки, байт

    bool     output_ready;      // если true -> можно отправлять в садок содержимое output_buffer
    uint8_t  output_buffer[256]; // этот буффер для отправки
    uint16_t output_length;     // длина подготовленных к отправке данных, байт;
    uint8_t  messages_in_outbuffer;

    // если придет подтверждение в виде команды 1 в регистр 0x1001 - инкрементировать счетчик
    uint16_t input_regs1[10]; // 0x1000

    uint16_t holding_regs1[32]; // 0x1000
    uint16_t holding_regs2[32]; // 0x2000
    uint16_t holding_regs8[32]; // 0x8000
}sadko;

void sadko_on_message(uint8_t* data, uint16_t len)
{
	int ret = modbus_on_rtu(sadko.uport, data, len);
	if(!ret)
    {
        uport_transmit(sadko.uport);
    }else{
	    errors_on_error(ret);
	    return;
	}
}

void sadko_set_modbus_addr(uint8_t addr)
{
    sadko.uport->addr = addr;
}

void start_sadko_task(void const *parameter)
{
    while(!sd_is_init() || !log_is_init())
        osDelay(1);

    for(;;)
    {
        uport_process(sadko.uport); // sadko
        sadko_update();
        osDelay(1);
    }
}

void save_output_position(void)
{
    uint8_t data[8];
    memcpy(data, &sadko.file_num, 2);
    memcpy(data, &sadko.file_read_ptr, 4);
    memcpy(data, &sadko.output_id, 2);
    sd_write_data(".sadko", (uint8_t*)data, 8, 0);
}

void sadko_recieved_approve(void)
{
    sadko.output_ready = false;
    sadko.output_length = 0;
    sadko.messages_in_outbuffer = 0;
    save_output_position();
}

int sadko_write_data(uint16_t* buffer, uint16_t from_addr, uint8_t num_regs)
{
    if((from_addr & 0xF000) == 0x1000)
    {
        memcpy(sadko.holding_regs1, buffer, num_regs * 2);
        // sadko.command call update read_index here !!!
        if(num_regs > 1)
        {
            uint16_t cmd = __REVSH(buffer[0]);
            uint16_t param = __REVSH(buffer[1]);
            if((cmd == 1) && (param == 1))
            {
                // TODO MOVE TO SADKO PROCESS, NOW TOO BIG TIMEOUT
                sadko_recieved_approve();
            }
        }
        // при подтверждении отправки
    }
    else if((from_addr & 0xF000) == 0x2000)
    {   
        // 0x2000 // uint16_t  Широта – градусы
        // 0x2001 // uint16_t  Широта – минуты
        // 0x2002 // uint16_t  Широта - десятитысячные доли минут
        // 0x2003 // uint16_t  Долгота – градусы
        // 0x2004 // uint16_t  Долгота – минуты
        // 0x2005 // uint16_t  Долгота - десятитысячные доли минут
        // 0x2006 // uint16_t  Код символа “E” для восточной или “W” для западной долготы
        // 0x2007 // uint16_t  Код символа “N” для северной или “S” для южной широты

        // 0x2008 int64_t      Текущее время GPS (формат UnixTime UTC), байты 1-2 (младшие)
        // 0x2009 int64_t      Текущее время GPS (формат UnixTime UTC), байты 3-4
        // 0x200A int64_t      Текущее время GPS (формат UnixTime UTC), байты 5-6
        // 0x200B int64_t      Текущее время GPS (формат UnixTime UTC), байты 7-8

        memcpy(sadko.holding_regs2, buffer, num_regs * 2);
    }
    else if((from_addr & 0xF000) == 0x8000)
    {   
        memcpy(sadko.holding_regs8, buffer, num_regs * 2);
    }

    return 0;
}

// int sadko_get_message(void)
// {
    // uint16_t offset = from_addr & 0xFFF;
    // if(offset > sadko.input_regs[MSG_AVAILABLE_REG])
        // return -1;

    // 0x2000 uint16_t Длина записи 1 (в байтах, включая номер записи) = n
    // 0x2001 uint16_t Запись данных 1 – номер записи = sadko.msg_id & 0xFFFF;
    // 0x2002 uint16_t Запись данных 1 – данные 1
    // 0x2003 uint16_t Запись данных 1 – данные 2
    // n uint16_t      Запись данных 1 – окончание данных
    // n+1 uint16_t    Длина записи 2
    // n+2 uint16_t    Запись данных 2 – номер записи
    // n+3 uint16_t    Запись данных 2 – данные 1
    // n+4 uint16_t    Запись данных 2 – данные 2

    // copy from buffer sadko.message; 
    // memcpy(buffer, &sadko.message[offset], num_regs * 2); // /*2 = sizeof(uint16_t*/
    // return 0;
// }

int sadko_read_input(uint16_t *buffer, uint16_t from_addr, uint8_t num_regs)
{
    // data address
    if((from_addr & 0xF000) == 0x2000)
    {
        memcpy(buffer, sadko.output_buffer, num_regs);
    }
    else if((from_addr & 0xF000) == 0x1000)
    {
        if(num_regs < 10)
        {
            uint16_t offset = from_addr & 0xFFF;
            memcpy(buffer, &sadko.input_regs1[offset], num_regs * 2); // /*2 = sizeof(uint16_t*/
        }
    }
    return 0;
}

int sadko_read_output_filenum(void)
{
    FRESULT res;
    DIR dir;
    FILINFO *fno = &sadko.fno;

    // lock mutex here? no
    uint8_t data[8];
    int ret = sd_read_data(".sadko", (uint8_t*)&data, 8, 0);
    if(!ret)
    {
        sadko.file_num      = data[0] | (data[1] << 8);
        sadko.file_read_ptr = data[2] | (data[3] << 8) | (data[4] << 16) | (data[5] << 24) ;
        sadko.output_id     = data[6] | (data[7] << 8);
    }
    char filename[64];

    sprintf(filename, "%s/welding_%04x.log", LOG_DIR, sadko.file_num);
    res = f_stat(filename, 0);
    if(res == FR_OK)
        return 0;
    if(res == FR_NO_FILE)
    {
        res = f_opendir(&dir, LOG_DIR);
        if(res != FR_OK)
        {
            return -16;
        }
        for(;;)
        {
            res = f_readdir(&dir, fno);
            if (res != FR_OK || fno->fname[0] == 0)
                break;
            if(!(fno->fattrib & AM_DIR))
            {
                if(!strncmp("welding_", fno->fname, 7))
                {
                    char* endptr;
                    uint32_t curr_file_num = strtol(&fno->fname[8], &endptr, 10);
                    sadko.file_num = curr_file_num;
                    sadko.output_id = 0;
                    save_output_position();
                    return 0;
                }
            }
        }
    }
    return -1;
}

uint32_t get_msg_id(uint8_t* msg)
{
    uint32_t id =   msg[8] |
                    msg[9] << 8 |
                    msg[10] << 16 |
                    msg[11] << 24;
    return id;
}

int check_file_non_empty(uint32_t filenum, char* filename)
{
    sprintf(filename, "%s/welding_%04lx.log", LOG_DIR, filenum & 0xFFFF);

    // find file with closest id
    if(filenum & 0xFFFF)
    {
        FILINFO *fno = &sadko.fno;
        FRESULT res = f_stat(filename, fno);
        if(res == FR_NO_FILE)
        {
            return -1;
        }
        if(!fno->fsize) // проверить размер файла
        {
            return -2;
        }
    }
    return 0;
}

int open_non_empty(FIL* file)
{
    const uint32_t input_file_num = log_current_file_num();
    uint16_t filenum = sadko.file_num;
    bool is_found = false;
    do
    {
        int ret = check_file_non_empty(filenum, sadko.filename);
        if(ret)
            filenum += 1;
        else{
            is_found = true;
            break;
        }
    }while(input_file_num > filenum);

    if(is_found)
        sadko.file_num = filenum;

    FRESULT res = f_open(file, sadko.filename, FA_READ);
    if(res != FR_OK) 
        return -1;
    return 0;
}

int check_end_of_file_and_switch(void)
{
    if(sadko.file.fptr == sadko.fno.fsize) // файл прочитан до конца
    {
        sadko.file_read_ptr = 0;
        sadko.file_num += 1;
        f_close(&sadko.file);
        sadko.is_file_opened = false;
        return 1;
    }
    return 0;
}

void update_input_regs(void)
{
    sadko.input_regs1[0] = 0x407;
    sadko.input_regs1[1] = 0x6;
    sadko.input_regs1[2] = 0;
    sadko.input_regs1[3] = (sadko.output_length + 1) / 2;
    
    sadko.input_regs1[4] = 0;
    sadko.input_regs1[5] = sadko.messages_in_outbuffer;
    sadko.input_regs1[6] = 1;
    sadko.input_regs1[7] = 0;

    sadko.input_regs1[8] = 0;
}

// id который не был отправлен
// если он был считан, но не переложен в output_buffer, 
// то считывать его не нужно
// тоесть перед чтением проверять id считанного сообщения !!!
// если сообщение уже было считано в прошлый раз, 
// то пропускаем этап считывания, пробуем его положить в outputbuffer

int fill_output_buffer(void)
{
    int ret = 0;
    int16_t free_bytes = 0;
    do{
        if(!sadko.is_file_opened)
        {
            int ret = open_non_empty(&sadko.file);
            if(!ret)
                sadko.is_file_opened = true;
            else
                break;
        }

        if(check_end_of_file_and_switch())
            continue;

        if(!sadko.message_length)
            ret = sd_read_message(&sadko.file, sadko.file_read_ptr, sadko.message, &sadko.message_length);

        free_bytes = SADKO_MAX_MSG_LEN - sadko.output_length - sadko.message_length - SADKO_MSG_HEADER_LEN;
        if(!ret && (free_bytes >= 0))
        {
            sadko.message_length += 2; // +номер записи(2байта)
            // пока есть место в output_buffer, кладем туда сообщения читая их из карты
            memcpy(&sadko.output_buffer[sadko.output_length], &sadko.message_length, 2);
            memcpy(&sadko.output_buffer[sadko.output_length + 2], &sadko.output_id, 2);
            memcpy(&sadko.output_buffer[sadko.output_length + 4], sadko.message, sadko.message_length);

            ++sadko.output_id; // увеличиваем строку
            ++sadko.messages_in_outbuffer;
            sadko.file_read_ptr = (uint32_t)sadko.file.fptr;
            sadko.output_length += sadko.message_length + 2; // + поле длины записи
            update_input_regs();
            // сообщение успешно переложено в outbuffer 
            // можно загружать следующее 
            sadko.message_length = 0;
        }else{
            break;
        }
    }while(free_bytes > 0);

    if(sadko.output_length > 0)
    {
        sadko.output_ready = true; // флаг
    }
    return 0;
}

int sadko_update(void)
{
    int ret = 0;
    if(sadko.output_ready) // буфер заполнен можно отдавать на запрос input_reg output_buffer
    {
        return 0;
    }
    // if(xSemaphoreTake(sdcard_mutex_handle(), portMAX_DELAY) == pdTRUE)
    if(1)
    {
        do{
            if(!sadko.file_num)
            {
                ret = sadko_read_output_filenum();
                if(ret)
                {
                    break;
                }
            }
            ret = fill_output_buffer();
            if(ret)
            {
                break;
            }
        }while(0);
        // xSemaphoreGive(sdcard_mutex_handle());
    }
    sadko.output_ready = true;
    return ret;
}

#ifndef UNITTEST
void sadko_data(char* string)
{
    sprintf(string, "output_id: %x (%d)\n"
                    "file: %s\n"
                    "fname: %s\n"
                    "size: %ld\n"
                    "readptr: %ld",
                     sadko.output_id,
                     sadko.output_id,
                     sadko.filename,
                     sadko.fno.fname,
                     (uint32_t)sadko.fno.fsize,
                     (uint32_t)sadko.file.fptr
                     );
}
#endif

int sadko_init(usart_port_t* port, uint8_t addr)
{
    // port read write callbacks
    sadko.uport = port;
    sadko.uport->addr = addr;
    sadko.uport->read_data = sadko_read_input;
    sadko.uport->write_data = sadko_write_data;
    sadko.output_id = 0;

    sadko.output_ready = false;  // если true -> можно отправлять сообщение в садок
    sadko.is_file_opened = false;

    return 0;
}
