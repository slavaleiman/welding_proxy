#include "main.h"
#include "fatfs.h"
#include "string.h"
#include "stdio.h"
#include "sdcard.h"
#include "welding_head.h"

struct 
{
    FATFS fs;
    SemaphoreHandle_t mutex_handle;
    StaticSemaphore_t mutex_buf;
    
    bool is_init;
}sdcard;

int sd_write_data(char* filename, uint8_t* data, uint16_t len, uint8_t flags)
{
    FIL file;
    FRESULT res;
    UINT bw;
    if(!flags)
        flags = FA_WRITE;
    do{
        res = f_open(&file, filename, flags);
        if(res != FR_OK) break;

        res = f_write (&file, (uint8_t*)data, len, &bw);
        if(res != FR_OK) break;

        res = f_printf(&file, "\r\n");
        if(res < 0) break;
    }while(0);

    f_close(&file);
    return (res >= 0) ? 0 : 1;
}

int sd_read_data(char* filename, uint8_t* data, uint16_t len, uint32_t from)
{
    FRESULT res;
    FIL file;
    UINT br;

    do{
        res = f_open(&file, filename, FA_READ | FA_OPEN_ALWAYS);
        if(res != FR_OK) break;
        res = f_lseek (&file, from);                             /* Move file pointer of the file object */
        if(res != FR_OK) break;
        f_read(&file, (TCHAR*)data, len, &br);
    }while(0);

    f_close(&file);
    return (res == FR_OK) ? 0 : 1;
}

int sd_read_message(FIL* file, uint32_t read_ptr, uint8_t* data, uint16_t* len)
{
    FRESULT res;
    uint32_t position = read_ptr;
    UINT br;
    uint32_t length;

    if(position)
    {
        res = f_lseek (file, position);
        if(res != FR_OK)
            return -1;
    }
    // timestamp + id + len
    res = f_read(file, (TCHAR*)data, MSG_HEADER_LEN, &br);
    if((res != FR_OK) || (!br)) 
        return -2;

    length = data[4] | data[5] << 8;
    if(length > 255)
        return -3;

    res = f_read(file, (TCHAR*)&data[MSG_HEADER_LEN], length, &br);
    if(res != FR_OK) return -4;
    *len = br + MSG_HEADER_LEN;
    return 0;
}

bool sd_is_init(void)
{
    return sdcard.is_init;
}

SemaphoreHandle_t sdcard_mutex_handle(void)
{
    return sdcard.mutex_handle;
}

void sd_init(void)
{
    sdcard.mutex_handle = xSemaphoreCreateMutexStatic(&sdcard.mutex_buf);
}

int sd_mount(void)
{
    FRESULT res = f_mount(&sdcard.fs, "", 1);
    if(res != FR_OK)
    {
        return -1;        
    }
    sdcard.is_init = true;
    return 0;
}
