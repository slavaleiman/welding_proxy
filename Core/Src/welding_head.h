#ifndef _WELDING_HEAD_H_
#define _WELDING_HEAD_H_ 1

#include "version.h"
#include "uptime.h"

#define MSG_HEADER_LEN 6 // 4-timestamp 2-len

#ifndef UNITTEST
#define WHEAD_BUFFER_SIZE (1024 * 3)
#else
#define WHEAD_BUFFER_SIZE (200)
#endif
#define DEV_ID 0x3331

#define FMW_VER (MODULE_VERSION_MAJOR * 100 + MODULE_VERSION_MINOR) 
enum{
	// DEVICE_ID = 0, 		// идентификатор устройства
	// FIRMWARE_VER = 1, 	// версия прошивки
	PROTO = 2,			// номер протокола. см. doc/Протокол Каховка.txt 
	NUM_STAGE_REG = 3, 	// код - номер этапа сварки, или номер участка(08 .. 50)
	POSITION_REG = 4,	// Мгновенное положение подвижной колонны в 0,1 мм
	AMPERAGE_REG = 5, 	// Мгновенный ток сварки в условных единицах аналогового входа 
	VOLTAGE_REG = 6,	// Мгновенное напряжение на первичных обмотках в условных единицах аналогового входа
	PRESSURE_REG = 7, 	// Мгновенное давление в условных единицах аналогового входа
	OPERATION_TIME_REG = 8, // Время от начала сварки в секундах
	WELDING_REGISTERS_NUM
} /* welding_reg_e */;

void whead_on_message(uint8_t* data, uint16_t len);
void welding_data(char* string);

#endif
