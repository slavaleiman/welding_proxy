
#include <stdint.h>

void memdump(uint8_t *data, uint16_t len, uint16_t str_size)
{
    for(uint16_t i = 0; i < len; ++i)
    {
        if(i && !(i % str_size))
        {
            printf("\n");
        }
        if(i < len)
        {
            printf("%02x ", data[i]);
        }
    }
    printf("\n\n");
}
