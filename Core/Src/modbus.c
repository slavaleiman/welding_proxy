
// адрес устройства         1 byte
// номер функции            1 byte
// Адрес регистра           2 byte
// Количество флагов        2 byte
// Количество byte данных   1 byte
// Данные                   ...
// CRC                      2 byte

#include "modbus.h"
#include "string.h"
#include "CRC.h"
#include "errors.h"
#include "usart_port.h"
// #include "modbus_port.h"

#define DEVICE_DEFAULT_ADDR     1

void modbus_inc_crc_err_rq(void)
{
    ++modbus.err_crc_rq;
}

void modbus_inc_handled_rq(void)
{
    ++modbus.handled_rq;
}

void modbus_inc_total_rq(void)
{
    ++modbus.total_rq;
}

void modbus_inc_send_err(void)
{
    ++modbus.err_send;
}

int8_t modbus_response_error(usart_port_t* uport, uint8_t* message, uint8_t error_code)
{
    uport->tx_buffer[0] = message[0];
    uport->tx_buffer[1] = message[1] | 0x80;
    uport->tx_buffer[2] = error_code;
    const uint16_t crc = CRC16(&uport->tx_buffer[0], 3);
    uport->tx_buffer[3] = crc & 0xFF;
    uport->tx_buffer[4] = crc >> 8;
    uport->tx_size = 5;
    return 0;
}

int8_t modbus_response_echo(usart_port_t* uport, uint8_t* message)
{
    uint16_t _crc = message[7] << 8 | message[6];
    uport->tx_buffer[0] = message[0];
    uport->tx_buffer[1] = 0x8;
    uport->tx_buffer[2] = 0;
    uport->tx_buffer[3] = 0;
    uport->tx_buffer[4] = message[4];
    uport->tx_buffer[5] = message[5];
    const uint16_t calc_crc = CRC16(&uport->tx_buffer[0], 6);
    if(calc_crc != _crc)
    {
        modbus_inc_crc_err_rq();
        return -1;
    }
    uport->tx_buffer[6] = calc_crc & 0xFF;
    uport->tx_buffer[7] = calc_crc >> 8;
    uport->tx_size = 8;
    return 0;
}

static int8_t get_input_reg(usart_port_t* uport, uint8_t* message)
{
    const uint16_t start_addr = message[2] << 8 | message[3];
    const uint16_t req_regs_num = message[4] << 8 | message[5];
    uint16_t _crc = message[6] | message[7] << 8;
    uint16_t calc_crc = CRC16(&message[0], 6);
    if(calc_crc != _crc)
    {
        modbus_inc_crc_err_rq();
        return 0;
    }
    uport->tx_buffer[0] = message[0];
    uport->tx_buffer[1] = 3;

    int8_t ret = 0;
    uint8_t count_reg = 0;

    uint16_t buffer[128];
    if(uport->read_data)
    {   // 0x2000 + [0 .. req_regs_num]
        ret = uport->read_data(buffer, start_addr, req_regs_num);
        if(ret < 0){
            ERROR(MODBUS_PORT_READ_ERROR);
            return -1;
        }
        count_reg = req_regs_num;
        memcpy(&uport->tx_buffer[3], buffer, count_reg);
    }else{
        ret = DATA_ADDR_ERR;
        ERROR(MODBUS_PORT_CANNOT_READ);
        return -1;
    }


    if(!count_reg)
        return ret;

    uport->tx_buffer[2] = count_reg * 2;
    const uint16_t crc = CRC16(&uport->tx_buffer[0], count_reg * 2 + 3);
    // fill tx buffer but not send
    uport->tx_buffer[3 + count_reg * 2] = crc & 0xFF;
    uport->tx_buffer[3 + count_reg * 2 + 1] = crc >> 8;
    uport->tx_size = count_reg * 2 + 5;
    return 0;
}

static int8_t get_holding_reg(usart_port_t* uport, uint8_t* message)
{
    const uint16_t start_addr = message[2] << 8 | message[3];
    const uint16_t req_regs_num = message[4] << 8 | message[5];
    uint16_t _crc = message[6] | message[7] << 8;
    uint16_t calc_crc = CRC16(&message[0], 6);
    if(calc_crc != _crc)
    {
        modbus_inc_crc_err_rq();
        return 0;
    }
    uport->tx_buffer[0] = message[0];
    uport->tx_buffer[1] = 3;

    uint8_t count_reg = 0;
    int8_t ret = 0;

    uint16_t buffer[128];

    if(uport->read_data)
    {
        ret = uport->read_data(buffer, start_addr, req_regs_num);
        if(ret >=0)
        {
            memcpy(&uport->tx_buffer[3], buffer, count_reg * 2);
            count_reg = req_regs_num;
        }
    }else{
        ret = DATA_ADDR_ERR;
        ERROR(MODBUS_PORT_CANNOT_READ);
        return -1;
    }

    if(!count_reg)
        return ret;

    uport->tx_buffer[2] = count_reg * 2;
    const uint16_t crc = CRC16(&uport->tx_buffer[0], count_reg * 2 + 3);
    // fill tx buffer but not send
    uport->tx_buffer[3 + count_reg * 2] = crc & 0xFF;
    uport->tx_buffer[3 + count_reg * 2 + 1] = crc >> 8;
    uport->tx_size = count_reg * 2 + 5;
    return 0;
}

// Read Holding Registers
int8_t modbus_f3(usart_port_t* uport,  uint8_t* message)
{
    uint8_t errcode = get_holding_reg(uport, message);
    if(errcode)
        modbus_response_error(uport, message, errcode);
    else
        modbus_inc_handled_rq();
    return 0;
}

// Read Holding Registers
int8_t modbus_f4(usart_port_t* uport,  uint8_t* message)
{
    uint8_t errcode = get_input_reg(uport, message);
    if(errcode)
        modbus_response_error(uport, message, errcode);
    else
        modbus_inc_handled_rq();
    return 0;
}

static int8_t write_single_reg(usart_port_t* uport, uint8_t* message)
{
    const uint16_t reg_addr    = message[2] << 8 | message[3];
    uint16_t write_value = message[4] << 8 | message[5];
    uint16_t _crc = message[6] | message[7] << 8;
    uint16_t calc_crc = CRC16(&message[0], 6);
    if(calc_crc != _crc)
    {
        modbus_inc_crc_err_rq();
        return 0;
    }
    int8_t ret = -1;
    if(uport->write_data)
    {
        ret = uport->write_data(&write_value, reg_addr, 1);
    }
    else{
        ret = -1;
        ERROR(MODBUS_PORT_CANNOT_WRITE);
    }

    if(ret)
        return ret;
    uport->tx_buffer[0] = message[0];
    uport->tx_buffer[1] = 6;
    uport->tx_buffer[2] = message[2];
    uport->tx_buffer[3] = message[3];
    uport->tx_buffer[4] = message[4];
    uport->tx_buffer[5] = message[5];
    uport->tx_buffer[6] = message[6];
    uport->tx_buffer[7] = message[7];
    uport->tx_size = 8;
    return 0;
}

int8_t modbus_f6(usart_port_t* uport, uint8_t* message)
{
    int8_t errcode = write_single_reg(uport, message);
    if(errcode)
        modbus_response_error(uport, message, errcode); 
    else
        modbus_inc_handled_rq();
    return 0;
}

static int8_t write_multiple_reg(usart_port_t* port, uint8_t* message, uint16_t size)
{
    const uint16_t start_addr = message[2] << 8 | message[3];
    const uint16_t regs_to_write = message[4] << 8 | message[5];
    const uint8_t bytes_to_write = message[6];

    uint16_t _crc = message[7 + bytes_to_write] | message[8 + bytes_to_write] << 8;
    uint16_t calc_crc = CRC16(&message[0], 7 + bytes_to_write);
    if(calc_crc != _crc)
    {
        modbus_inc_crc_err_rq();
        return 0;
    }

    port->tx_buffer[0] = message[0];
    port->tx_buffer[1] = 16;

    uint8_t count_reg = 0;
    {
        const uint8_t index = 7 + count_reg * 2;
        if(index + 1 > size - 2)
        {
            return -1;
        }
        // uint16_t reg_value = message[index] << 8 | message[index + 1];
        if(port->write_data)
        {

            int8_t ret = port->write_data((uint16_t*)&message[index], start_addr, regs_to_write);
            if(ret)
                return -1;
        }else{
            return -1;
        }
        // если записался хоть один регистр - то
        // при ошибке записи в следующий, просто выходим
        // и отправляем количество успешно записанных регистров
        count_reg = regs_to_write;
    }    

    if(!count_reg) // если не записалось ниодного - ошибка в запросе
        return REQUEST_ERR;
    port->tx_buffer[2] = message[2];
    port->tx_buffer[3] = message[3];
    port->tx_buffer[4] = count_reg >> 8;
    port->tx_buffer[5] = count_reg & 0xFF;

    const uint16_t crc = CRC16(&port->tx_buffer[0], 6);
    port->tx_buffer[6] = crc & 0xFF;
    port->tx_buffer[7] = crc >> 8;
    port->tx_size = 8;
    return 0;
}

int8_t modbus_f16(usart_port_t* uport, uint8_t* message, uint8_t size)
{
    int8_t errcode = write_multiple_reg(uport, message, size);
    if(errcode)
        modbus_response_error(uport, message, errcode);
    else
        modbus_inc_handled_rq();
    return 0;
}

int8_t modbus_on_rtu(usart_port_t* port, uint8_t* message, uint16_t size)
{
    const uint8_t addr = message[0];
    if(addr != port->addr)
        return 1;

    modbus_inc_total_rq();

    if(size < 8)
        return 1;

    const uint8_t func_num = message[1];
    switch(func_num)
    {
        case 3: // чтение значений из нескольких регистров хранения (Read Holding Registers).
            return modbus_f3(port, message);
        case 4: // чтение значений из нескольких регистров ввода (Read Input Registers).
            return modbus_f4(port, message);
        case 6: // запись значения в один регистр хранения
            return modbus_f6(port, message);
        case 8: // Диагностика
            return modbus_response_echo(port, message);
        case 16: // запись значений в несколько регистров хранения (Preset Multiple Registers)
            return modbus_f16(port, message, size);
        default:
            modbus_response_error(port, message, FUNC_CODE_ERR);
    }

    return 0;
}

// WARNING MAY BE NEED MUTEX HERE!
// void modbus_check_trasmition(usart_port_t* uport, size_t len)
// {
//     for(uint8_t i = 0; i < len; ++i)
//     {
//         if(uport->dma_rx_buffer[i] ^ uport->tx_buffer[i])
//         {
//             modbus_inc_send_err();
//             return;
//         }
//     }
// }
