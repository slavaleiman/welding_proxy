
#include "uptime.h"
#include "main.h"
#include <stdio.h>
struct
{
	uint32_t start_time;
	uint32_t count;
    uint32_t count_ovr;
    uint32_t operation_time;
}uptime_mod;

uint32_t uptime_update_time(void)
{
    uint32_t counter = uwTick / 1000;
    if(counter < uptime_mod.count) // если произошло переполнение
    {
        ++uptime_mod.count_ovr;
    }
    uptime_mod.count = counter;
    uptime_mod.operation_time = uptime_mod.start_time + uptime_mod.count + uptime_mod.count_ovr * (0xFFFFFFFF / 1000); // секунд
    return uptime_mod.operation_time;
}

uint32_t uptime(void)
{
    return uptime_mod.operation_time;
}

void uptime_string(char* string)
{
    uint32_t counter = uwTick / 1000;
    if(counter < uptime_mod.count)
    {
        ++uptime_mod.count_ovr;
    }
    uptime_mod.count = counter;

    uint32_t cnt = uptime_mod.start_time  + uptime_mod.count + (uptime_mod.count_ovr * 0xFFFFFFFF / 1000); // секунд

    uint32_t h = (uint32_t)(cnt / 3600);
    uint8_t m = (cnt - h * 3600) / 60;
    uint8_t s = cnt - h * 3600 - m * 60;
    sprintf(string, "Время работы: %lu:%02u:%02u", h, m, s);
}
