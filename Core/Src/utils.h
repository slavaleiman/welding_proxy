#ifndef _UTILS_H_
#define _UTILS_H_
#include <stdint.h>

void memdump(uint8_t *data, uint16_t len, uint16_t str_size);

#endif
