#ifndef _ROM_DATA_
#define _ROM_DATA_ 1

#include "main.h"
#include <stdbool.h>

#define OPTIONS_IN_FLASH  1
#ifdef OPTIONS_IN_FLASH

// Sector 12 0x0810 0000 - 0x0810 3FFF 16 Kbytes
// Sector 13 0x0810 4000 - 0x0810 7FFF 16 Kbytes
// Sector 14 0x0810 8000 - 0x0810 BFFF 16 Kbytes
// Sector 15 0x0810 C000 - 0x0810 FFFF 16 Kbytes

    #define DATA_SECTOR1       		12 // FLASH_Sector_12
    #define DATA_SECTOR2       		13
    #define DATA_SECTOR3       		14
    #define DATA_SECTOR4       		15
    #define ROM_DATA_CONFIG_ADDR 	(0x08100000)
#endif

void rom_load_data(void);
void rom_save_data(void);

void rom_data_changed(void);
bool rom_data_is_changed(void);

void rom_update_all_params(void);
void rom_write_time(void);

#endif //_ROM_DATA_
