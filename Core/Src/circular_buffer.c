#include <string.h>
#include "circular_buffer.h"
#include "errors.h"
#include <stdint.h>
#include <stdio.h>

int cbuffer_push(circular_buffer_t *cbuff, uint8_t *item, size_t size)
{
    if(cbuff->count + size > cbuff->maxlen)
    {
        return -1;
    }
    
    if(cbuff->head + size < cbuff->buffer_end)
        memcpy(cbuff->head, item, size);
    else{
        int skip = cbuff->buffer_end - cbuff->head;
        memcpy(cbuff->head, item, skip);
        memcpy(cbuff->buffer, &item[skip], size - skip);
    }

    cbuff->head = (char*)cbuff->head + size;
    if(cbuff->head >= cbuff->buffer_end)
        cbuff->head -= cbuff->maxlen;

    cbuff->count += size;
    return 0;
}

int cbuffer_push_message(circular_buffer_t *cbuff, uint8_t* data, uint16_t len)
{
    if(!len)
        return -1;
    uint32_t size = sizeof(message_t) + len;
    if(cbuff->count + size > cbuff->maxlen)
    {
        return -2;
    }
    message_t mess;
    mess.data = cbuff->head + sizeof(message_t);
    if((uint32_t)mess.data >= (uint32_t)cbuff->buffer_end)
    {
        mess.data -= cbuff->maxlen;
#ifdef UNITTEST
        printf("----------1: buffer: %x\thead:%x\tdata: %x\ttail: %x\tend:%x\n", (int)cbuff->buffer, (int)cbuff->head, (int)mess.data, (int)cbuff->tail, (int)cbuff->buffer_end);
#endif
    }
#ifdef UNITTEST
        printf("2: buffer: %x\thead:%x\tdata: %x\ttail: %x\tend:%x\n", (int)cbuff->buffer, (int)cbuff->head, (int)mess.data, (int)cbuff->tail, (int)cbuff->buffer_end);
#endif
    mess.len = len;
    if(cbuffer_push(cbuff, (uint8_t*)&mess, sizeof(message_t)))
    {
        return -3;
    }
    if(cbuffer_push(cbuff, data, len))
    {
        return -4;
    }
    return 0;
}

int cbuffer_pop(circular_buffer_t *cbuff, uint8_t *item, size_t size)
{
    if(!cbuff->count || (cbuff->count < size))
    {
        return -1;
    }

    uint32_t skip = 0;
    if(cbuff->tail + size <= cbuff->buffer_end)
    {
        memcpy(item, cbuff->tail, size);
        // TODO check ret value
    }else{
        skip = cbuff->buffer_end - cbuff->tail;
        memcpy(item, cbuff->tail, skip);
        // TODO check ret value
        memcpy(&item[skip], cbuff->buffer, size - skip);
        // TODO check ret value
    }

    cbuff->tail = (char*)cbuff->tail + size;
    if(cbuff->tail >= cbuff->buffer_end)
        cbuff->tail -= cbuff->maxlen;
    cbuff->count -= size;
    return 0;
}

// return length of readed message
int cbuffer_pop_message(circular_buffer_t *cbuff, uint8_t* data)
{
    message_t in_mess;
    // если circular buffer не пуст
    int ret = cbuffer_pop(cbuff, (uint8_t*)&in_mess, sizeof(message_t));
    if(!ret)
    {
        if(in_mess.len > 255)
        {
            // errors_on_error(CBUFFER_POP_ERROR);
            // TODO FLUSH CBUFFER ON ERROR
            return -1;
        }
        if(cbuff->tail != in_mess.data)
        {
            // errors_on_error(CBUFFER_POP_ERROR);
            // TODO FLUSH CBUFFER ON ERROR
            return -1;
        }
        if(!cbuffer_pop(cbuff, data, in_mess.len))
        {
#ifdef UNITTEST
            printf("input data: %x\n", (int)in_mess.data);
#endif
            return in_mess.len;
        }
    }
    // else if(ret < 0)
    // {
    //     return ret;
    // }
    return 0;
}

// when comes second part of message
// its tail = data 
int cbuffer_check_data(circular_buffer_t *cbuff, uint8_t* data)
{
    if(cbuff->tail != data)
        return -1;
    return 0;
}
