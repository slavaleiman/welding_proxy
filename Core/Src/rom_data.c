
#include "errors.h"
#include <string.h>
#include "main.h"
#include "CRC.h"

#define ROM_OPTIONS_SECTOR  FLASH_SECTOR_12 // start of bank 2
#define ROM_OPTIONS_SIZE    10 // 8 + 2 = data + CRC
#define ROM_OPTIONS_ADDR    0x08100000
#define ROM_DATA_ADDR       0x08120000// sector 17 for save input log

// можно использовать 7 секторов размером 128К

struct
{
    uint8_t     buffer[ROM_OPTIONS_SIZE]; // for read options
    uint32_t    start_time;
    uint32_t    counter;
    uint32_t    count_ovr;
    bool        is_empty;
    uint32_t    read_skip;
    struct {
        uint32_t bytes_written; // позиция в буфере
        uint32_t time;
    }options;
}rom_data;

uint32_t operation_time(void)
{
    uint32_t counter = HAL_GetTick() / 1000;
    if(counter < rom_data.counter) // если произошло переполнение
    {
        ++rom_data.count_ovr;
    }
    rom_data.counter = counter;
    uint32_t operation_time = rom_data.start_time + rom_data.counter + rom_data.count_ovr * (0xFFFFFFFF / 1000); // секунд
    return operation_time;
}

static void write_options(void)
{
    rom_data.options.time = operation_time();
    uint8_t* data = (uint8_t*)&rom_data.options;
    const uint32_t size = sizeof(rom_data.options);

    uint32_t addr = ROM_OPTIONS_ADDR;
    HAL_FLASH_Unlock(); // unlock flash writing
    do{
        FLASH_Erase_Sector(ROM_OPTIONS_SECTOR, FLASH_VOLTAGE_RANGE_3);

        __HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_EOP | FLASH_FLAG_PGAERR | FLASH_FLAG_WRPERR | FLASH_SR_PGSERR);
        HAL_StatusTypeDef status = HAL_OK;
        for (uint32_t i = 0; i < size; ++i)
        {
            status = HAL_FLASH_Program(FLASH_TYPEPROGRAM_BYTE, addr + i, data[i]);
            if(status != HAL_OK)
                break;
        }
    }while(0);
    HAL_FLASH_Lock(); // lock the flash
}

int rom_write(uint8_t* data, uint32_t len)
{
    int ret = 0;
    uint32_t from_addr = ROM_DATA_ADDR + rom_data.options.bytes_written;
    HAL_FLASH_Unlock(); // unlock flash writing
    do{
        // NO ERASE
        __HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_EOP | FLASH_FLAG_PGAERR | FLASH_FLAG_WRPERR | FLASH_SR_PGSERR);
        HAL_StatusTypeDef status = HAL_OK;
        for (uint32_t i = 0; i < len; ++i)
        {
            status = HAL_FLASH_Program(FLASH_TYPEPROGRAM_BYTE, from_addr + i, data[i]);
            if(status != HAL_OK)
            {
                ret = -1;
                break;
            }
            ++rom_data.options.bytes_written;
        }
    }while(0);
    HAL_FLASH_Lock(); // lock the flash

    write_options(); // save write position + time
    return ret;
}

static void rom_read(uint8_t* data, uint32_t size)
{
    uint32_t from = rom_data.read_skip;
    uint32_t i;
    for(i = 0; i < size; ++i)
        data[i] = *(uint8_t*)(ROM_OPTIONS_ADDR + from + i);
    rom_data.read_skip += size;
}

void read_options(void)
{
    uint8_t data[ROM_OPTIONS_SIZE];
    for(uint32_t i = 0; i < ROM_OPTIONS_SIZE; ++i)
        data[i] = *(uint8_t*)(ROM_OPTIONS_ADDR + i);

    const uint16_t read_crc = (data[ROM_OPTIONS_SIZE - 2] << 8) | data[ROM_OPTIONS_SIZE - 1];
    const uint16_t calc_crc = CRC16(data, ROM_OPTIONS_SIZE - 2);

    if(read_crc != calc_crc)
    {
        rom_data.is_empty = true;
        // memset(data, 0, ROM_OPTIONS_SIZE - 2);
        errors_on_error(ROM_CRC_ERROR);
    }else{
        memcpy(&rom_data.options, data, ROM_OPTIONS_SIZE - 2);
    }
}

void rom_init(void)
{
    memset(rom_data.buffer, 0, ROM_OPTIONS_SIZE);
    memset(&rom_data.options, 0, sizeof(rom_data.options));
    read_options();
}
