#!/usr/bin/python3
# -*- coding: iso-8859-15 -*-

import time
import serial
from modbus import *
import sys
import struct

if len(sys.argv) < 2:
    print("USAGE: %s [port] [baudrate] [count]" % sys.argv[0])
    sys.exit(1)

ser = serial.Serial(sys.argv[1], sys.argv[2], timeout=3.0)

msgnum = int(sys.argv[3])
if not msgnum:
    msgnum = 1
num = 0

for i in range(msgnum):
    if(ser.isOpen() == False):
        ser.open()

    # output_string = "01 02 03 04 05 06 07 08"
    # output_string = "01 02 03 04 05 06 07 08 09 10"
    output_string   = "01 02 03 04 05 06 07 08 09 10 11"
    cmd_bytes = bytearray.fromhex(output_string)
    num += 1
    print(str(num) + " -> " + str(cmd_bytes))
    ser.write(cmd_bytes)

    time.sleep(0.03)
    # time.sleep(0.033)
