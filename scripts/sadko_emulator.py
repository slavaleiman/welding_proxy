#!/usr/bin/python3
# -*- coding: iso-8859-15 -*-

import time
import serial
from modbus import *
import sys
import struct

if len(sys.argv) < 2:
    print("USAGE: %s [port] [baudrate] [count]" % sys.argv[0])
    sys.exit(1)

set_port(sys.argv[1], sys.argv[2])

msgnum = int(sys.argv[3])
if not msgnum:
    msgnum = 1
ADDR = 0x20

def get_msg_size(mb_response):
    # A1  A0  Q1  Q0  N   D (N байт)
    print(mb_response)
    print(mb_response[24:29].replace(" ",""))
    data_available = int(mb_response[24:29].replace(" ",""), 16)
    return data_available

for i in range(msgnum):
    ret = read_input_reg(ADDR, 0x1000, 8, sleep_time=0.1)
    out = ret[1]
    data_items_count = get_msg_size(out)
    print("item count : " + str(data_items_count))

    if(data_items_count):
        input_bytes = read_input_reg(ADDR, 0x2000, data_items_count, 0.1)
        print(input_bytes[1])
        
        # approve
        ret = write_multiple_regs(ADDR, 0x1000, (1,1), sleep_time=0.1)
        print(ret)

    time.sleep(1)
