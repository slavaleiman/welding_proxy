#!/usr/bin/python
# -*- coding: utf-8 -*-

import time
import serial
import sys
import struct
from modbus import *

if len(sys.argv) < 2:
    print("USAGE: %s [port]" % sys.argv[0])
    sys.exit(1)

ser = serial.Serial(sys.argv[1], sys.argv[2])
#ser = serial.Serial(sys.argv[1], baudrate=1200)

if(ser.isOpen() == False):
    ser.open()

count = 0

while 1 :
    out = ''
    seq = []
    time.sleep(0.09)

    # modbus read
    while ser.inWaiting() > 0:
        for c in ser.read():
            seq.append(c)
            out = ''.join(("%02X " % v) for v in seq)
            if chr(c) == '\n':
                break
    if len(out) > 26:
        count += 1
        output = 'DEVICE_ID:     {:2}\n'.format(out[21:26])+\
                 'FIRMWARE_VER:  {:2}\n'.format(out[27:32])+\
                 'PROTO:         {:2}\n'.format(out[33:38])+\
                 'NUM_STAGE_REG: {:2}\n'.format(out[39:44])+\
                 'POSITION_REG:  {:2}\n'.format(out[45:50])+\
                 'AMPERAGE_REG:  {:2}\n'.format(out[51:56])+\
                 'VOLTAGE_REG:   {:2}\n'.format(out[57:62])+\
                 'PRESSURE_REG:  {:2}\n'.format(out[63:68])+\
                 'OPERATION_TIME_REG: {:2}\n'.format(out[69:74])

                # PROTO = 2,          // номер протокола. см. doc/Протокол Каховка.txt 
                # NUM_STAGE_REG = 3,  // код - номер этапа сварки, или номер участка(08 .. 50)
                # POSITION_REG = 4,   // Мгновенное положение подвижной колонны в 0,1 мм
                # AMPERAGE_REG = 5,   // Мгновенный ток сварки в условных единицах аналогового входа 
                # VOLTAGE_REG = 6,    // Мгновенное напряжение на первичных обмотках в условных единицах аналогового входа
                # PRESSURE_REG = 7,   // Мгновенное давление в условных единицах аналогового входа
                # OPERATION_TIME_REG = 8, // Время от начала сварки в секундах
                # WELDING_REGISTERS_NUM

        system('clear')
        print(out)
        print(output)

