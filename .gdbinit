target extended localhost:4242
set print pretty on
set pagination off

define step_mult
    set $step_mult_max = 1000
    if $argc >= 1
        set $step_mult_max = $arg0
    end

    set $step_mult_count = 0
    while ($step_mult_count < $step_mult_max)
        set $step_mult_count = $step_mult_count + 1
        printf "step #%d\n", $step_mult_count
        step
    end
end

define pbuff 
	$buff = $arg1
	$len = $arg2
	
	print $buff[0]
	print $buff[2]

end

define pusart1
	p/x *USART1
	p/x *DMA2_Stream2
	p/x *DMA2_Stream7
	p/x DMA2
end

define p2
	p/x *USART2
	p/x *DMA1_Stream5
	p/x *DMA1_Stream6
	p/x DMA1
end

#b usart_irq_handler
#b open_log_file
#b sadko_read_output_id
#b sd_read_data
#b cbuffer_pop_message
#b write_message

#b file_sync
#b fill_output_buffer
#b sd_read_message
#b check_file_non_empty

b whead_on_message
b sd_read_message
b sadko_read_input
