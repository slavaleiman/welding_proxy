EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_ST_STM32F4:STM32F429ZITx U?
U 1 1 6078FCF9
P 3500 4150
F 0 "U?" H 3500 461 50  0000 C CNN
F 1 "STM32F429ZITx" H 3500 370 50  0000 C CNN
F 2 "Package_QFP:LQFP-144_20x20mm_P0.5mm" H 2500 750 50  0001 R CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/DM00071990.pdf" H 3500 4150 50  0001 C CNN
	1    3500 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 6650 1850 6650
Text Label 1850 6650 0    50   ~ 0
UART7_TX
Wire Wire Line
	2300 6750 1850 6750
Text Label 1850 6750 0    50   ~ 0
UART7_RX
$EndSCHEMATC
