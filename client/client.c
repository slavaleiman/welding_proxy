#include <stdio.h>   /* Standard input/output definitions */
#include <string.h>  /* String function definitions */
#include <unistd.h>  /* UNIX standard function definitions */
#include <fcntl.h>   /* File control definitions */
#include <errno.h>   /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */
#include <sys/select.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#define SELECT_TIMEOUT 5000 // micro second

int set_interface_attribs(int fd, int speed)
{
    struct termios tty;

    if (tcgetattr(fd, &tty) < 0) {
        printf("Error from tcgetattr: %s\n", strerror(errno));
        return -1;
    }

    cfsetospeed(&tty, (speed_t)speed);
    cfsetispeed(&tty, (speed_t)speed);

    tty.c_cflag |= (CLOCAL | CREAD);    /* ignore modem controls */
    tty.c_cflag &= ~CSIZE;
    tty.c_cflag |= CS8;         /* 8-bit characters */
    tty.c_cflag &= ~PARENB;     /* no parity bit */
    tty.c_cflag &= ~CSTOPB;     /* only need 1 stop bit */
    tty.c_cflag &= ~CRTSCTS;    /* no hardware flowcontrol */

    /* setup for non-canonical mode */
    tty.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);
    tty.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
    tty.c_oflag &= ~OPOST;

    /* fetch bytes as they become available */
    tty.c_cc[VMIN] = 1;
    tty.c_cc[VTIME] = 1;

    if (tcsetattr(fd, TCSANOW, &tty) != 0) {
        printf("Error from tcsetattr: %s\n", strerror(errno));
        return -1;
    }
    return 0;
}

int main(int argc, char** argv)
{
	int fd;
	char p[256];
	int n;
	char* dev_name = "/dev/ttyACM0";
	if(argc > 1)
		dev_name = argv[1];
	fd = open(dev_name, O_RDWR | O_NOCTTY);
	if (fd == -1)
	{
		printf("Open_port: Unable to open %s %s\n", dev_name, strerror(errno));
		return -1;
	}
	set_interface_attribs(fd, B115200);
	fd_set rfds;
	struct timeval tv;

	printf("Port Opened Successfully\n");
	do{
		printf("Enter coordinate delta: ");
		errno = 0;
		scanf("%s", p);
		if (errno != 0) {
			perror("scanf");
			break;
		}
		n = strlen(p) + 1;

		if(n == 1 && p[0] == 'q')
			break;
		int ret  = write(fd, p, n);
		if (ret < 0)
		{
			fputs("Write() failed!\n", stderr);
			break;
		}
		unsigned i = 0;

		if(!FD_ISSET(fd, &rfds))
		{
			FD_ZERO(&rfds);
			FD_SET(fd, &rfds);
			tv.tv_sec = 0;
			tv.tv_usec = SELECT_TIMEOUT;
		}

	    tcdrain(fd);    /* delay for output */

	    /* simple noncanonical input */
	    do {
	        unsigned char buf[80];
	        int rdlen;

	        rdlen = read(fd, buf, sizeof(buf) - 1);
	        if (rdlen > 0) {
	#if 1 //def DISPLAY_STRING
	            buf[rdlen] = 0;
	            printf("Read %d: \"%s\"\n", rdlen, buf);
	            break;
	#else /* display hex */
	            unsigned char   *p;
	            printf("Read %d:", rdlen);
	            for (p = buf; rdlen-- > 0; p++)
	                printf(" 0x%x", *p);
	            printf("\n");
	#endif
	        } else if (rdlen < 0) {
	            printf("Error from read: %d: %s\n", rdlen, strerror(errno));
	        } else {  /* rdlen == 0 */
	            printf("Timeout from read\n");
	        }               
	        /* repeat read to get full message */
	    } while (1);

		// every time select clean rfds and tv and we need to set up it again every time
		// ret = select(fd + 1, &rfds, NULL, NULL, &tv); 
		// if(ret == -1)
		// {
		// 	perror("select()");
		// 	break;
		// }else if(ret)
		// {
		// 	// printf("Data is available now. ret = %d\n", ret);
		// 	ret = read(fd, &p[i], 55); // read echo
		// 	if(ret >= 0)
		// 	{
		// 		i += n;
		// 		if(!ret)
		// 		{
		// 			printf("has been read %d symbols: %s \n", i, p);
		// 			// break;
		// 		}
		// 	}
		// 	else{
		// 		printf("Read error: %s %d %d\n", strerror(errno), errno, ret);
		// 		if(errno == EAGAIN)
		// 		{
		// 			printf("EAGAIN ret=%d\n", ret);
		// 			continue;
		// 		}
		// 	}
		// }

	}while(1);
	close(fd);
	return 1;
}
